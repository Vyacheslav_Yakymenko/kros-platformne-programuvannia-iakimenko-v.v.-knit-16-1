public class Main {

    public static int getCountOfLargestElement(int[] arr)
    {
        int max = arr[0], count = 0;
        for (int i = 0; i < arr.length; i++){
            if (arr[i] > max)
                max = arr[i];
        }

        for(int i = 0; i < arr.length; i++){
            if (arr[i] == max)
                count++;
        }

        return count;
    }

    public static void main(String[] args) {
        int[] arr = {8, 2, 8, 4, 5, 1, 1, 8};
        int result = getCountOfLargestElement(arr);

        System.out.println(result);
    }

}
