import java.util.Arrays;
import java.util.Collections;

class SomeClass{
    private int[][][] arr;

    SomeClass()
    {
        arr = new int[][][] { {{7, 1}, {4, 4}}, {{8, 2}, {7, 7}} };
    }

    void sortDimensions(){
        for(int i = 0; i < arr.length; i++) {
            Arrays.sort(arr[i], Collections.reverseOrder());
        }
    }
}

public class Main {

    public static void main(String[] args) {
        var test = new SomeClass();
        test.sortDimensions();
    }

}
