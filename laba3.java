import java.util.Scanner;

public class Main {

    public static int getSameLetterWordsCount(String str) {
        var arr = str.split(" ");
        var count = 0;

        for (var word : arr) {
            var temp = word.split("");
            if (temp[0].equals(temp[temp.length - 1])) {
                count++;
            }
        }

        return count;
    }


    public static void main(String[] args) {
        var s = new Scanner(System.in);
        System.out.print("Enter a string: ");
        var str = s.nextLine();
        var result = getSameLetterWordsCount(str);
        System.out.println("Words with same letter at the beginning and end: " + result);
    }

}
