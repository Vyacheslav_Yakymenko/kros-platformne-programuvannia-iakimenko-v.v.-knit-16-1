import java.io.*;

public class Main {
    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("c:/text.txt"));
        String line = br.readLine();
        br.close();
        System.out.println(line);

        BufferedWriter bw = new BufferedWriter(new FileWriter("c:/text.txt"));
        String newLine = toTitleCase(line);
        System.out.println(newLine);
        bw.write(newLine);
        bw.close();
    }
}
