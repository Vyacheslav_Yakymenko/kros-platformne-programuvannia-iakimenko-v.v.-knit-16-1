class BookArrThread extends Thread{
    private BookArray arr;
    public BookArrThread(BookArray arr){
        this.arr=arr;
    }
    public void run(){
        arr.sort();
        arr.removeDuplicates();
        arr.Show();
    }
}