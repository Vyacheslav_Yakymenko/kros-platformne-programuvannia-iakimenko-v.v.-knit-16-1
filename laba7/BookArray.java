import java.io.*;
import java.util.*;

class BookArray implements Serializable{
    private ArrayList<Book> books;
    private static final long serialVersionUID = 1L;
    public BookArray(ArrayList<Book> books){
        this.books=books;
    }
    public BookArray(){
        books=new ArrayList<Book>();
    }
    public void add(Book book){
        books.add(book);
    }
    public void remove(int index){
        books.remove(index);
    }
    public void sort(){
        Collections.sort(books);
    }
    public void clear(){
        books.clear();
    }
    public void removeDuplicates(){
        ArrayList<Book> result=new ArrayList<Book>();
        for(Book b : books){
            if(!result.contains(b)){
                result.add(b);
            }
        }
        books=result;
    }
    public void  Show(){
        for(Book book : books){
            System.out.printf("Name:%s\tCode:%d\n",book.getName(),book.getCode());
        }
    }
}