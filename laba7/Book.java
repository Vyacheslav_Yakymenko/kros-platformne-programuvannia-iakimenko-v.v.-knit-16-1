import java.io.*;

class Book implements Comparable<Book>, Serializable{
    private String name;
    private int code;
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public void setCode(int code){
        this.code=code;
    }
    public int getCode(){
        return code;
    }
    public Book(String name,int code){
        this.name=name;
        this.code=code;
    }
    @Override
    public boolean equals(Object obj) {
        var b=(Book) obj;
        if(this.name==b.getName() && this.code==b.getCode())
            return true;
        else
            return false;
    }
    @Override
    public int hashCode() {
        return name != null? name.hashCode()*37+code:code;
    }
    public int compareTo(Book secondBook){
        var secondCode=secondBook.getCode();
        if(code<secondCode)
            return -1;
        else if(code>secondCode)
            return 1;
        else
            return 0;
    }
}