import java.util.*;
import java.io.*;

class Program{
    public static void main(String[] args) {
        try{
            var books = new BookArray();
            ObjectInputStream ois=new ObjectInputStream(new FileInputStream("books.dat"));
            books=(BookArray)ois.readObject();
            ois.close();
            var t=new BookArrThread(books);      
            t.start();
            t.join();
        }
        catch(Exception ex){
            System.out.print(ex.getMessage());
        }
    }
}