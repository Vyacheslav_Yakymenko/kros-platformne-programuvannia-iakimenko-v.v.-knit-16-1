public class Main {

    public static void main(String[] args) {
        int product = 0;
        for (var arg : args) {
            int num = Integer.parseInt(arg);
            if (num % 3 == 0) {
                if (product == 0)
                    product += num;
                else
                    product *= num;
            }
        }
        System.out.println(product);
    }

}
